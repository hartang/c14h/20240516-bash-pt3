#!/usr/bin/env bash
set -euo pipefail

declare -A ARGS=(
    [port]=8080
)

function _usage {
    cat <<EOF
usage: $0 [OPTION...]

Options:
  -p, --port        port to bind to (default: ${ARGS[port]})
  -h, --help        show this help text
EOF
}

while [[ $# -gt 0 ]]; do
    case "$1" in
        "-h"|"--help")
            _usage
            exit 0
            ;;
        -*)
            echo "unknown option '$1'" 1>&2
            _usage 1>&2
            exit 1
            ;;
        *)
            echo "unknown keyword '$1'" 1>&2
            _usage 1>&2
            exit 1
            ;;
    esac
done
