#!/usr/bin/env bash
set -euo pipefail

declare -i VERBOSITY=0
while [[ $# -gt 0 ]]; do
    case "$1" in
        "-v"|"--verbose")
            VERBOSITY=$((VERBOSITY + 1))
            shift
            ;;
        -*)
            echo "unknown option '$1'" 1>&2
            exit 1
            ;;
        *)
            echo "unknown keyword '$1'" 1>&2
            exit 1
            ;;
    esac
done

echo "verbosity: $VERBOSITY"
