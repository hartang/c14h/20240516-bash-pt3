#!/usr/bin/env bash
set -euo pipefail

while [[ $# -gt 0 ]]; do
    case "$1" in
        -*)
            echo "unknown option '$1'" 1>&2
            exit 1;;
        *)
            echo "unknown keyword '$1'" 1>&2
            exit 1;;
    esac
done
