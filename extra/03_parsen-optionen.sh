#!/usr/bin/env bash
set -euo pipefail

declare -i PORT=0
function _fatal {
    echo "FATAL: $*" 1>&2
    exit 1
}

while [[ $# -gt 0 ]]; do
    case "$1" in
        "-p"|"--port")
            [[ $# -gt 1 ]] || _fatal "'$1' requires value PORT"
            PORT=$2
            [[ $PORT -ge 1 ]] && [[ $PORT -le 65535 ]] ||
                _fatal "PORT must be between 1 and 65535"
            shift 2
            ;;
        -*)
            echo "unknown option '$1'" 1>&2
            exit 1
            ;;
        *)
            echo "unknown keyword '$1'" 1>&2
            exit 1
            ;;
    esac
done

echo "port is: $PORT"
