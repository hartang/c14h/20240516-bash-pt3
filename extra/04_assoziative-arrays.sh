#!/usr/bin/env bash
set -euo pipefail

declare -A ARGS=(
    [port]=8080
    [verbose]=0
    [force]=false
)

# Wert zuweisen:
ARGS[logfile]="/tmp/other.log"

# Wert auslesen:
LOGFILE="${ARGS[logfile]}"
echo "logfile is: '$LOGFILE'"

# Debugging:
echo "Arguments are:"
echo "${ARGS[@]@K}"
