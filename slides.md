---
title: Bash und Ich (Teil 3)
author: hartan
patat:
  breadcrumbs: true
  wrap: true
  slideLevel: 2
  margins:
    left: 5
    right: 5
    top: 1
  eval:
    figlet:
      command: figlet -s -w 70 -f big
      fragment: false
      replace: true
...

```figlet
Bash und Ich
( Teil 3 )
```

# CLI-Interfaces

- Vorherige Vorträge:
    - <https://gitlab.com/hartang/c14h/20240418-bash-pt1>
    - <https://gitlab.com/hartang/c14h/20240425-bash-pt2>


## Motivation

- Bash-Skripte wiederverwendbar machen

. . .

- Simpler Ansatz: *Variablen im Skript*
    - (**+**) Können dokumentiert werden
    - (**-**) Erfordern Änderungen am Quellcode
    ```bash
    PARAM_A="Ein Wert"
    PARAM_B=42
    ```

- *Oder*: CLI-Interface


# Argumente parsen

## Bausteine

- `$#`: Anzahl CLI-Argumente (*argc*)
- `$@`: Alle CLI-Argument als String
- `$n`: *n*-tes CLI-Argument
- `shift`: Entfernt vorderstes Argument (*pop*)

## Grundstruktur

```bash
while [[ $# -gt 0 ]]; do
    case "$1" in
        -*)
            echo "unknown option '$1'" 1>&2
            exit 1;;
        *)
            echo "unknown keyword '$1'" 1>&2
            exit 1;;
    esac
done
```

## Flags

```bash
case "$1" in
    "-v"|"--verbose")
        VERBOSITY=$((VERBOSITY + 1))
        shift
        ;;
    #...
esac
```

- **Wichtig**: `shift` löscht das bearbeitete Argument
- *Sonst* läuft der Check in Endlosschleife!


## Optionen

```bash
declare -i PORT=0
case "$1" in
    "-p"|"--port")
        [[ $# -gt 1 ]] || _fatal "'$1' requires value PORT"
        PORT=$2
        [[ $PORT -ge 1 ]] && [[ $PORT -le 65535 ]] ||
            _fatal "PORT must be between 1 and 65535"
        shift 2
        ;;
    #...
esac
```


# Argumente speichern

## Assoziative Arrays

- Bündel einzelner Variablen schnell unübersichtlich
- Definiert in bash mit `declare -A`:
  ```bash
  declare -A ARGS=(
      [port]=8080
      [verbose]=0
      [force]=false
  )
  ```
- Erzeugt eine Variable mit mehreren Feldern
- *Einschränkung*: Kann keine Arrays speichern

## Arbeiten mit assoziativen Arrays

```bash
# Wert zuweisen:
ARGS[logfile]="/tmp/other.log"

# Wert auslesen:
LOGFILE="${ARGS[logfile]}"

# Debugging:
echo "${ARGS[@]@K}"
```


# Hilfetext

## Hilfetext-Funktion

```bash
function _usage {
    cat <<EOF
usage: $0 [OPTION...]

Options:
  -p, --port        port to bind to (default: ${ARGS[port]})
  -v, --verbose     show verbose ouptut
EOF
}
```

## CLI-Argument

```bash
case "$1" in
    "-h"|"--help")
        _usage
        exit 0
        ;;
    #...
esac
```


# Fragen?

```figlet
EOF
```
