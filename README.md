# Bash und Ich - CLI Interfaces

Heute zeige ich euch, wie ich CLIs für Bash-Skripte implementiere. Im Detail
geht es um das (naive) parsen von CLI-Argumenten, das komfortable Ablegen der
Argumente mit assoziativen Arrays sowie Tipps zum schreiben eines Hilfe-Textes.

Hier befinden sich die "Folien" zu einem Vortrag, den ich am 16.05.2024 im
Rahmen der c14h beim NoName e.V. gehalten habe. Die Aufnahme zum Vortrag ist
hier verlinkt: https://www.noname-ev.de/chaotische_viertelstunde.html#c14h_617


## Ausführen

Die Präsentation wird in einem Container ausgeführt. Dazu benötigt man `podman`
oder `docker` auf dem Rechner. Der folgende Befehl lädt dann den Container
herunter oder baut ihn lokal und startet die Präsentation:

```
$ ./present.sh
```
